﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// find solution for "One Tough Puzzle"
// my first c# program!
// Alex Slater - 20/4/2009

namespace PuzzleCracker
{

    class Program
    {

        static bool[,] AllowedLeftOf = new bool[36, 36];       // we need a lookup table to see which piece is allowed to be left of another piece
        static bool[,] AllowedAbove = new bool[36, 36];       // we need a lookup table to see which piece is allowed to be above another piece
        static Piece[] JigsawPieces;
        
        static void Main(string[] args)
        {

            JigsawPieces = new Piece[36];       // there are 36 pieces: 9 physical pieces each in 4 orientations
            char[] PieceId = new char[36]; // = "abcdefghijklmnopqrstuvwxyz0123456789";

            string left;
            string right;


            // create array of chars, one of which is used to identify each piece
            InitPieceIDs(PieceId);

            // create the original 9 pieces
            InitOriginalPieces(PieceId);

            // create the rest of the pieces from rotating the original 9 pieces
            InitRotatedPieces(PieceId);

            // create lookup table of which pieces fit into the piece to the right of them
            InitLeftCheckArray(AllowedLeftOf);

            // create lookup table of which pieces fit into the piece beneath them
            InitAboveCheckArray(AllowedAbove);

            left = "";
            right = new String(PieceId );

            Console.WriteLine("Starting...");
            SolvePuzzle(left, right);
            Console.WriteLine("Done.");
         
        }


        // fill PieceID array with A-Z then whatever 10 chars come after those...
        static void InitPieceIDs(char[] PieceId)
        {
            for (int i = 0; i < 36; i++)
                PieceId[i] = System.Convert.ToChar('A' + i);

        }


        static public void InitLeftCheckArray(bool[,] myAllowedLeftOf)
        { 

            for (int leftpiece = 0; leftpiece <36; leftpiece++)
                for (int check=0; check< 36; check++)
                    if (
                        (leftpiece != check) &                                                          // piece can't be left of itself
                        (JigsawPieces[leftpiece].Suits[1] == JigsawPieces[check].Suits[3]) &            // if suits match
                        (JigsawPieces[leftpiece].Shapes[1] != JigsawPieces[check].Shapes[3])            // and one is peg and the other hole (or vice versa)
                        )  
                        {
                            myAllowedLeftOf[leftpiece,check] = true;                                    // then left piece is allowed left of check piece
                        }
        }

        static public void InitAboveCheckArray(bool[,] myAllowedAbove)
        {

            for (int abovepiece = 0; abovepiece < 36; abovepiece++)
                for (int check = 0; check < 36; check++)
                    if (
                        (abovepiece != check) &
                        (JigsawPieces[abovepiece].Suits[2] == JigsawPieces[check].Suits[0]) &
                        (JigsawPieces[abovepiece].Shapes[2] != JigsawPieces[check].Shapes[0])
                        )
                    {
                        myAllowedAbove[abovepiece, check] = true;
                    }
        }

        // create the original 9 pieces
        static public void InitOriginalPieces(char[] PieceId)
        {

            int offset = 0;

            JigsawPieces[offset] = new Piece(PieceId[offset],
                                Piece.Suit.Spade, 
                                Piece.Suit.Spade, 
                                Piece.Suit.Heart, 
                                Piece.Suit.Club,
                                Piece.Shape.Peg, 
                                Piece.Shape.Peg, 
                                Piece.Shape.Hole, 
                                Piece.Shape.Hole);
            offset++;

            JigsawPieces[offset] = new Piece(PieceId[offset],
                                Piece.Suit.Club,
                                Piece.Suit.Heart, 
                                Piece.Suit.Diamond, 
                                Piece.Suit.Club,
                                Piece.Shape.Peg, 
                                Piece.Shape.Peg, 
                                Piece.Shape.Hole, 
                                Piece.Shape.Hole);
            offset++;

            JigsawPieces[offset] = new Piece(PieceId[offset],
                                Piece.Suit.Club,
                                Piece.Suit.Club,
                                Piece.Suit.Diamond, 
                                Piece.Suit.Diamond,
                                Piece.Shape.Peg,
                                Piece.Shape.Hole, 
                                Piece.Shape.Hole,
                                Piece.Shape.Peg);
            offset++;

            JigsawPieces[offset] = new Piece(PieceId[offset],
                                Piece.Suit.Heart,
                                Piece.Suit.Spade, 
                                Piece.Suit.Heart,
                                Piece.Suit.Club,
                                Piece.Shape.Peg,
                                Piece.Shape.Hole,
                                Piece.Shape.Hole, 
                                Piece.Shape.Peg);
            offset++;

            JigsawPieces[offset] = new Piece(PieceId[offset],
                                Piece.Suit.Heart, 
                                Piece.Suit.Diamond, 
                                Piece.Suit.Diamond, 
                                Piece.Suit.Heart,
                                Piece.Shape.Peg,
                                Piece.Shape.Peg, 
                                Piece.Shape.Hole, 
                                Piece.Shape.Hole);
            offset++;

            JigsawPieces[offset] = new Piece(PieceId[offset],
                                Piece.Suit.Heart, 
                                Piece.Suit.Diamond, 
                                Piece.Suit.Club, 
                                Piece.Suit.Club,
                                Piece.Shape.Peg, 
                                Piece.Shape.Peg, 
                                Piece.Shape.Hole, 
                                Piece.Shape.Hole);
            offset++;

            JigsawPieces[offset] = new Piece(PieceId[offset],
                                Piece.Suit.Spade, 
                                Piece.Suit.Spade, 
                                Piece.Suit.Club, 
                                Piece.Suit.Heart,
                                Piece.Shape.Peg, 
                                Piece.Shape.Hole, 
                                Piece.Shape.Hole, 
                                Piece.Shape.Peg);
            offset++;

            JigsawPieces[offset] = new Piece(PieceId[offset],
                                Piece.Suit.Diamond, 
                                Piece.Suit.Heart, 
                                Piece.Suit.Diamond, 
                                Piece.Suit.Spade,
                                Piece.Shape.Peg, 
                                Piece.Shape.Hole, 
                                Piece.Shape.Hole, 
                                Piece.Shape.Peg);
            offset++;

            JigsawPieces[offset] = new Piece(PieceId[offset],
                                Piece.Suit.Spade, 
                                Piece.Suit.Diamond, 
                                Piece.Suit.Spade, 
                                Piece.Suit.Heart,
                                Piece.Shape.Peg, 
                                Piece.Shape.Peg, 
                                Piece.Shape.Hole, 
                                Piece.Shape.Hole);
            offset++;

        }

        static public void InitRotatedPieces(char[] PieceId)
        {

            // now rotate those 9 pieces
            // rotate all 9 pieces using first orientation, then second... etc

            int newpiece = 9;                     // offset of first piece we're creating via rotation code

            for (int orientation = 1; orientation < 4; orientation++)
                for (int apiece = 0; apiece < 9; apiece++)
                {
                    InitRotatedPiece(JigsawPieces, apiece, orientation, newpiece, PieceId);
                    newpiece++;
                }

        }

        // create a single piece, given:
        // the array of pieces
        // the piece we're changing the orientation of
        // how we're orientating this new piece
        // the offset of the piece we're creating
        // we add four before doing the modulo to handle negative values correctly
        // ie 2-3 = -1, and we want the answer to be 3, so we do (2-3)+4 % 4
        static public void InitRotatedPiece(Piece[] blah, int apiece, int orientation, int newpiece, char[] PieceId)
        {

            blah[newpiece] = new Piece(PieceId [newpiece],
                                        blah[apiece].Suits[((0 - orientation) + 4) % 4],
                                        blah[apiece].Suits[((1 - orientation) + 4) % 4],
                                        blah[apiece].Suits[((2 - orientation) + 4) % 4],
                                        blah[apiece].Suits[((3 - orientation) + 4) % 4],
                                        blah[apiece].Shapes[((0 - orientation) + 4) % 4],
                                        blah[apiece].Shapes[((1 - orientation) + 4) % 4],
                                        blah[apiece].Shapes[((2 - orientation) + 4) % 4],
                                        blah[apiece].Shapes[((3 - orientation) + 4) % 4]);
        
        
        }
        

        // recursive call
        // recursve through every possible 9 char combination of the initial value of right
        static void SolvePuzzle(string left, string right)
        {

            // if we've got 9 chars in left then we have a possible solution
            if (left.Length == 9)
            {

                
                // the final position must have 6 pegs sticking out and 6 holes on the edges too
                if
                    ((JigsawPieces[left[0] - 65].ShapeValue[0] +
                    JigsawPieces[left[1] - 65].ShapeValue[0] +
                    JigsawPieces[left[2] - 65].ShapeValue[0] +
                    JigsawPieces[left[2] - 65].ShapeValue[1] +
                    JigsawPieces[left[5] - 65].ShapeValue[1] +
                    JigsawPieces[left[8] - 65].ShapeValue[1] +
                    JigsawPieces[left[8] - 65].ShapeValue[2] +
                    JigsawPieces[left[7] - 65].ShapeValue[2] +
                    JigsawPieces[left[6] - 65].ShapeValue[2] +
                    JigsawPieces[left[6] - 65].ShapeValue[3] +
                    JigsawPieces[left[3] - 65].ShapeValue[3] +
                    JigsawPieces[left[0] - 65].ShapeValue[3] == 0))
                {

                    // it's possible that the same piece is used more than once in the solution,
                    // because I create 4 different orientations of the same piece.  we need to make sure
                    // we only consider solutions where each jigsaw piece is only used once, regardless of orientation
                    // replace each number in this string with a period.
                    string fcheck = "012345678";
                    for (int i = 0; i < 9; i++)
                    {
                        fcheck = fcheck.Replace(System.Convert.ToString((left[i] - 65) % 9), ".");
                    }

                    
                    // if we have only periods, we've found the solution
                    if (fcheck == ".........")
                        Console.WriteLine(left + " = " +
                        (left[0] - 65) % 9 + "/" + (left[0] - 65) / 9 + " " +
                        (left[1] - 65) % 9 + "/" + (left[1] - 65) / 9 + " " +
                        (left[2] - 65) % 9 + "/" + (left[2] - 65) / 9 + " " +
                        (left[3] - 65) % 9 + "/" + (left[3] - 65) / 9 + " " +
                        (left[4] - 65) % 9 + "/" + (left[4] - 65) / 9 + " " +
                        (left[5] - 65) % 9 + "/" + (left[5] - 65) / 9 + " " +
                        (left[6] - 65) % 9 + "/" + (left[6] - 65) / 9 + " " +
                        (left[7] - 65) % 9 + "/" + (left[7] - 65) / 9 + " " +
                        (left[8] - 65) % 9 + "/" + (left[8] - 65) / 9);

                  
                }
            
            }
            else
            {
                for (int i = 0; i < right.Length; i++)
                {

                    bool bAllowed = false;
                    string newleft = left + right.Substring(i, 1);

                    switch (newleft.Length)
                    {
                        //case 0:
                        //    bAllowed = true;
                        //    break;
                        case 1:
                            bAllowed = true;
                            break;

                        case 2:
                            bAllowed = AllowedLeftOf[newleft[0]-65, newleft[1] - 65];
                            break;

                        case 3:
                            bAllowed = AllowedLeftOf[newleft[1] - 65, newleft[2] - 65];
                            break;
                        
                        case 4:
                            bAllowed = AllowedAbove[newleft[0] - 65, newleft[3] - 65];
                            break;
                        
                        case 5:
                            bAllowed = AllowedAbove[newleft[1] - 65, newleft[4] - 65]
                            &
                            AllowedLeftOf[newleft[3] - 65, newleft[4] - 65];
                            break;
                        
                        case 6:
                            bAllowed = AllowedAbove[newleft[2] - 65, newleft[5] - 65]
                            &
                            AllowedLeftOf[newleft[4] - 65, newleft[5] - 65];
                            break;
                        
                        case 7:
                            bAllowed = AllowedAbove[newleft[3] - 65, newleft[6] - 65];
                            break;
                        
                        case 8:
                            bAllowed = AllowedAbove[newleft[4] - 65, newleft[7] - 65]
                            &
                            AllowedLeftOf[newleft[6] - 65, newleft[7] - 65];
                            break;
                        
                        case 9:
                            bAllowed = AllowedAbove[newleft[5] - 65, newleft[8] - 65]
                            &
                            AllowedLeftOf[newleft[7] - 65, newleft[8] - 65];
                            break;
                        }

                    if (bAllowed)
                    {
                        //Console.WriteLine("checking: " + newleft);
                        SolvePuzzle(newleft, right.Remove(i, 1));
                    }
                    
                }
            }
        }


    }  //end of class Program


} // end of namespace PuzzleCracker

