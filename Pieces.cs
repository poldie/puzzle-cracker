﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PuzzleCracker
{
    class Piece
    {
        public enum Suit {Heart, Spade, Club, Diamond};
        public enum Shape { Peg, Hole };        

        const int NUMBER_OF_EDGES= 4;

        // ctor for Piece
        public Piece(char myid, Suit suit1, Suit suit2, Suit suit3, Suit suit4, Shape shape1, Shape shape2, Shape shape3, Shape shape4)
        {
            Id = myid;

            Suits = new Suit[NUMBER_OF_EDGES];
            Suits[0] = suit1;
            Suits[1] = suit2;
            Suits[2] = suit3;
            Suits[3] = suit4;

            Shapes = new Shape[NUMBER_OF_EDGES];
            Shapes[0] = shape1;
            Shapes[1] = shape2;
            Shapes[2] = shape3;
            Shapes[3] = shape4;

            ShapeValue = new int[NUMBER_OF_EDGES];
            ShapeValue[0] = shape1 == Shape.Hole ? -1 : 1;
            ShapeValue[1] = shape2 == Shape.Hole ? -1 : 1;
            ShapeValue[2] = shape3 == Shape.Hole ? -1 : 1;
            ShapeValue[3] = shape4 == Shape.Hole ? -1 : 1;
        
        }

        char id;                     // identifier for this piece
        public char Id
        {
            get { return id; }
            set { id = value; }
        }

        Suit[] suits;           // array of 4 'suits' - whether the holes or bits that stick out on each edge are diamonds, hearts etc
        internal Suit[] Suits
        {
            get { return suits; }
            set { suits = value; }
        }

        Shape[] shapes;         // whether each of the 4 intersections stick out or are holes pegs
        internal Shape[] Shapes
        {
            get { return shapes; }
            set { shapes = value; }
        }

        int[] shapeValue;       // can't do maths on enums so store shapes where holes are zero and pegs are 1
        public int[] ShapeValue
        {
            get { return shapeValue; }
            set { shapeValue = value; }
        }


    }
}
